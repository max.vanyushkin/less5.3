resource "google_compute_instance" "junewayvm" {
  name         = var.machine_name
  machine_type = var.machine_type
  zone         = var.zone

  tags = ["vanyushkinm", "juneway", "enable-ports-s"]

  metadata = {
    ssh-keys = "root:${file(var.public_key_path)}"
  }

  connection {
    type = "ssh"
    user = "root"

    private_key = file(var.private_key_path)
    host        = self.network_interface[0].access_config[0].nat_ip
  }


  # Installing Docker
  provisioner "remote-exec" {
    inline = [
      "curl -sSL https://get.docker.com/ | sh",
      "usermod -aG docker `echo $USER`",
      "mkdir -p /data",
      "echo Juneway ${self.name} ${self.network_interface[0].access_config[0].nat_ip} > /data/index.html",
      "docker run -d -p 80:80 -v /data:/usr/share/nginx/html nginx:latest"
    ]
  }

  boot_disk {
    initialize_params {
      image = var.image
      size  = var.disk_size
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.external_ip.address
    }
  }
}

resource "google_compute_attached_disk" "st-attached" {
  disk     = google_compute_disk.st_disk.id
  instance = google_compute_instance.junewayvm.id
}

resource "google_compute_attached_disk" "nd-attached" {
  disk     = google_compute_disk.nd_disk.id
  instance = google_compute_instance.junewayvm.id
}

resource "google_compute_disk" "st_disk" {
  name = "st-disk"
  size = 8
  type = "pd-standard"
  zone = var.zone
}

resource "google_compute_disk" "nd_disk" {
  name = "nt-disk"
  size = 10
  type = "pd-standard"
  zone = var.zone
}

resource "google_compute_address" "external_ip" {
  name = "ip"
}

locals {
  ports = var.ports_to_be_opened
}

resource "google_compute_firewall" "enable-ports-s" {
  name          = "enable-ports-s"
  network       = "default"
  description   = "Creates firewall rule targeting tagged instances"
  priority      = 1
  source_ranges = ["0.0.0.0/0"]


  dynamic "allow" {
    for_each = var.ports_to_be_opened
    content {
      protocol = "tcp"
      ports    = [allow.value]
    }
  }
  source_tags = ["enable-http-s"]
}

resource "cloudflare_record" "juneway_record" {
  zone_id = var.cloudflare_zone_id
  name    = "larryunderwood.juneway.pro"
  value   = google_compute_address.external_ip.address
  type    = "A"
  ttl     = 3600
}